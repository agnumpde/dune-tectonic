#ifndef DUNE_TECTONIC_GLOBALFRICTIONDATA_HH
#define DUNE_TECTONIC_GLOBALFRICTIONDATA_HH

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/tectonic/frictiondata.hh>

template <class DomainType>
double evaluateScalarFunction(
    Dune::VirtualFunction<DomainType, Dune::FieldVector<double, 1>> const &f,
    DomainType const &x) {
  Dune::FieldVector<double, 1> ret;
  f.evaluate(x, ret);
  return ret;
};

template <int dimension> class GlobalFrictionData {
public:
  FrictionData operator()(Dune::FieldVector<double, dimension> const &x) const {
    return FrictionData(C(), L(), V0(), evaluateScalarFunction(a(), x),
                        evaluateScalarFunction(b(), x), mu0());
  }

protected:
  using VirtualFunction =
      Dune::VirtualFunction<Dune::FieldVector<double, dimension>,
                            Dune::FieldVector<double, 1>>;

  double virtual const &C() const = 0;
  double virtual const &L() const = 0;
  double virtual const &V0() const = 0;
  VirtualFunction virtual const &a() const = 0;
  VirtualFunction virtual const &b() const = 0;
  double virtual const &mu0() const = 0;
};
#endif
