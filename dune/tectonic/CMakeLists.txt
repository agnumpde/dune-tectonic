install(FILES
  body.hh
  frictiondata.hh
  frictionpotential.hh
  globalfrictiondata.hh
  globalfriction.hh
  globalratestatefriction.hh
  gravity.hh
  localfriction.hh
  minimisation.hh
  myblockproblem.hh
  mydirectionalconvexfunction.hh
  quadraticenergy.hh
  tectonic.hh
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/tectonic)
