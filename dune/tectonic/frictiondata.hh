#ifndef DUNE_TECTONIC_FRICTIONDATA_HH
#define DUNE_TECTONIC_FRICTIONDATA_HH

#include <dune/common/parametertree.hh>

struct FrictionData {
  explicit FrictionData(double _C, double _L, double _V0, double _a, double _b,
                        double _mu0)
      : C(_C), L(_L), V0(_V0), a(_a), b(_b), mu0(_mu0) {}

  double const C;
  double const L;
  double const V0;
  double const a;
  double const b;
  double const mu0;
};
#endif
