#ifndef DUNE_TECTONIC_BODY_HH
#define DUNE_TECTONIC_BODY_HH

template <int dimension> struct Body {
  using ScalarFunction =
      Dune::VirtualFunction<Dune::FieldVector<double, dimension>,
                            Dune::FieldVector<double, 1>>;
  using VectorField =
      Dune::VirtualFunction<Dune::FieldVector<double, dimension>,
                            Dune::FieldVector<double, dimension>>;

  double virtual getPoissonRatio() const = 0;

  double virtual getYoungModulus() const = 0;

  ScalarFunction virtual const &getShearViscosityField() const = 0;
  ScalarFunction virtual const &getBulkViscosityField() const = 0;

  ScalarFunction virtual const &getDensityField() const = 0;
  VectorField virtual const &getGravityField() const = 0;
};
#endif
