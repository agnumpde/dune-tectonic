#ifndef DUNE_TECTONIC_MYBLOCKPROBLEM_HH
#define DUNE_TECTONIC_MYBLOCKPROBLEM_HH

// Based on dune/tnnmg/problem-classes/blocknonlineartnnmgproblem.hh

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/fmatrixev.hh>

#include <dune/fufem/arithmetic.hh>
#include <dune/solvers/common/interval.hh>
#include <dune/solvers/computeenergy.hh>
#include <dune/tnnmg/problem-classes/bisection.hh>
#include <dune/tnnmg/problem-classes/blocknonlineargsproblem.hh>

#include <dune/tectonic/globalfriction.hh>
#include <dune/tectonic/minimisation.hh>
#include <dune/tectonic/mydirectionalconvexfunction.hh>
#include <dune/tectonic/quadraticenergy.hh>

/** \brief Base class for problems where each block can be solved with a
 * modified gradient method */
template <class ConvexProblem>
class MyBlockProblem : /* not public */ BlockNonlinearGSProblem<ConvexProblem> {
private:
  typedef BlockNonlinearGSProblem<ConvexProblem> Base;

public:
  using typename Base::ConvexProblemType;
  using typename Base::LocalMatrixType;
  using typename Base::LocalVectorType;
  using typename Base::MatrixType;
  using typename Base::VectorType;

  size_t static const block_size = ConvexProblem::block_size;
  size_t static const coarse_block_size = block_size;

  /** \brief Solves one local system */
  class IterateObject;

  struct Linearization {
    size_t static const block_size = coarse_block_size;

    using LocalMatrix = typename MyBlockProblem<ConvexProblem>::LocalMatrixType;
    using MatrixType = Dune::BCRSMatrix<typename Linearization::LocalMatrix>;
    using VectorType =
        Dune::BlockVector<Dune::FieldVector<double, Linearization::block_size>>;
    using BitVectorType = Dune::BitSetVector<Linearization::block_size>;

    typename Linearization::MatrixType A;
    typename Linearization::VectorType b;
    typename Linearization::BitVectorType ignore;

    Dune::BitSetVector<Linearization::block_size> truncation;
  };

  MyBlockProblem(Dune::ParameterTree const &parset, ConvexProblem &problem)
      : Base(parset, problem),
        maxEigenvalues_(problem.f.size()),
        localBisection(0.0, 1.0, 0.0, true, 0.0) {
    for (size_t i = 0; i < problem.f.size(); ++i) {
      LocalVectorType eigenvalues;
      Dune::FMatrixHelp::eigenValues(problem.A[i][i], eigenvalues);
      maxEigenvalues_[i] =
          *std::max_element(std::begin(eigenvalues), std::end(eigenvalues));
    }
  }

  std::string getOutput(bool header = false) const {
    if (header) {
      outStream.str("");
      for (size_t j = 0; j < block_size; ++j)
        outStream << "  trunc" << std::setw(2) << j;
    }
    std::string s = outStream.str();
    outStream.str("");
    return s;
  }

  double computeEnergy(const VectorType &v) const {
    return 0.0; // FIXME
    // return ::computeEnergy(problem_.A, v, problem_.f) + problem_.phi(v);
  }

  void projectCoarseCorrection(VectorType const &u,
                               typename Linearization::VectorType const &v,
                               VectorType &projected_v,
                               Linearization const &linearization) const {
    projected_v = v;
    for (size_t i = 0; i < v.size(); ++i)
      for (size_t j = 0; j < block_size; ++j)
        if (linearization.truncation[i][j])
          projected_v[i][j] = 0;
  }

  double computeDampingParameter(VectorType const &u,
                                 VectorType const &projected_v) const {
    VectorType v = projected_v;

    double const vnorm = v.two_norm();
    if (vnorm <= 0)
      return 1.0;

    v /= vnorm; // Rescale for numerical stability

    auto const psi = restrict(problem_.A, problem_.f, u, v, problem_.phi);
    Dune::Solvers::Interval<double> D;
    psi.subDiff(0, D);
    if (D[1] > 0) // NOTE: Numerical instability can actually get us here
      return 0;

    int bisectionsteps = 0;
    Bisection const globalBisection; // NOTE: defaults
    return globalBisection.minimize(psi, vnorm, 0.0, bisectionsteps) / vnorm;
  }

  void assembleTruncate(VectorType const &u, Linearization &linearization,
                        Dune::BitSetVector<block_size> const &ignore) const {
    // we can just copy the ignore information
    linearization.ignore = ignore;

    // determine truncation pattern
    linearization.truncation.resize(u.size());
    linearization.truncation.unsetAll();
    for (size_t i = 0; i < u.size(); ++i) {
      if (problem_.phi.regularity(i, u[i]) > 1e8) { // TODO: Make customisable
        linearization.truncation[i] = true;
        continue;
      }

      for (size_t j = 0; j < block_size; ++j)
        if (linearization.ignore[i][j])
          linearization.truncation[i][j] = true;
    }

    // construct sparsity pattern for linearization
    Dune::MatrixIndexSet indices(problem_.A.N(), problem_.A.M());
    indices.import(problem_.A);
    problem_.phi.addHessianIndices(indices);

    // construct matrix from pattern and initialize it
    indices.exportIdx(linearization.A);
    linearization.A = 0.0;

    // compute quadratic part of hessian (linearization.A += problem_.A)
    for (size_t i = 0; i < problem_.A.N(); ++i) {
      auto const end = std::end(problem_.A[i]);
      for (auto it = std::begin(problem_.A[i]); it != end; ++it)
        linearization.A[i][it.index()] += *it;
    }

    // compute nonlinearity part of hessian
    problem_.phi.addHessian(u, linearization.A);

    // compute quadratic part of gradient
    linearization.b.resize(u.size());
    problem_.A.mv(u, linearization.b);
    linearization.b -= problem_.f;

    // compute nonlinearity part of gradient
    problem_.phi.addGradient(u, linearization.b);

    // -grad is needed for Newton step
    linearization.b *= -1.0;

    // apply truncation to stiffness matrix and rhs
    for (size_t row = 0; row < linearization.A.N(); ++row) {
      auto const col_end = std::end(linearization.A[row]);
      for (auto col_it = std::begin(linearization.A[row]); col_it != col_end;
           ++col_it) {
        size_t const col = col_it.index();
        for (size_t i = 0; i < col_it->N(); ++i) {
          auto const blockEnd = std::end((*col_it)[i]);
          for (auto blockIt = std::begin((*col_it)[i]); blockIt != blockEnd;
               ++blockIt)
            if (linearization.truncation[row][i] or
                linearization.truncation[col][blockIt.index()])
              *blockIt = 0.0;
        }
      }
      for (size_t j = 0; j < block_size; ++j)
        if (linearization.truncation[row][j])
          linearization.b[row][j] = 0.0;
    }
    for (size_t j = 0; j < block_size; ++j)
      outStream << std::setw(9) << linearization.truncation.countmasked(j);
  }

  /** \brief Constructs and returns an iterate object */
  IterateObject getIterateObject() {
    return IterateObject(localBisection, problem_, maxEigenvalues_);
  }

private:
  std::vector<double> maxEigenvalues_;

  // problem data
  using Base::problem_;

  Bisection const localBisection;

  mutable std::ostringstream outStream;
};

/** \brief Solves one local system using a scalar Gauss-Seidel method */
template <class ConvexProblem>
class MyBlockProblem<ConvexProblem>::IterateObject {
  friend class MyBlockProblem;

protected:
  /** \brief Constructor, protected so only friends can instantiate it
   * \param bisection The class used to do a scalar bisection
   * \param problem The problem including quadratic part and nonlinear part
   */
  IterateObject(Bisection const &bisection, ConvexProblem const &problem,
                std::vector<double> const &maxEigenvalues)
      : problem(problem),
        maxEigenvalues_(maxEigenvalues),
        bisection_(bisection) {}

public:
  /** \brief Set the current iterate */
  void setIterate(VectorType &u) {
    this->u = u;
    return;
  }

  /** \brief Update the i-th block of the current iterate */
  void updateIterate(LocalVectorType const &ui, size_t i) {
    u[i] = ui;
    return;
  }

  /** \brief Minimise a local problem
   * \param[out] ui The solution
   * \param m Block number
   * \param ignore Set of degrees of freedom to leave untouched
   */
  void solveLocalProblem(
      LocalVectorType &ui, size_t m,
      typename Dune::BitSetVector<block_size>::const_reference ignore) {
    {
      LocalVectorType localb = problem.f[m];
      auto const end = std::end(problem.A[m]);
      for (auto it = std::begin(problem.A[m]); it != end; ++it) {
        size_t const j = it.index();
        Arithmetic::subtractProduct(localb, *it, u[j]); // also the diagonal!
      }
      Arithmetic::addProduct(localb, maxEigenvalues_[m], u[m]);

      // We minimise over an affine subspace
      for (size_t j = 0; j < block_size; ++j)
        if (ignore[j])
          localb[j] = 0;
        else
          ui[j] = 0;

      QuadraticEnergy<
          typename ConvexProblem::NonlinearityType::LocalNonlinearity>
          localJ(maxEigenvalues_[m], localb, problem.phi.restriction(m));
      minimise(localJ, ui, bisection_);
    }
  }

private:
  ConvexProblem const &problem;
  std::vector<double> maxEigenvalues_;
  Bisection const bisection_;
  // state data for smoothing procedure used by:
  // setIterate, updateIterate, solveLocalProblem
  VectorType u;
};
#endif
