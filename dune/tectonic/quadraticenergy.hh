#ifndef DUNE_TECTONIC_QUADRATICENERGY_HH
#define DUNE_TECTONIC_QUADRATICENERGY_HH

#include <memory>

template <class NonlinearityTEMPLATE> class QuadraticEnergy {
public:
  using Nonlinearity = NonlinearityTEMPLATE;
  using LocalVector = typename Nonlinearity::VectorType;

  QuadraticEnergy(double alpha, LocalVector const &b, Nonlinearity const &phi)
      : alpha(alpha), b(b), phi(phi) {}

  double const alpha;
  LocalVector const &b;
  Nonlinearity const &phi;
};
#endif
