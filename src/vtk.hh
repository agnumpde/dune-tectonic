#ifndef SRC_VTK_HH
#define SRC_VTK_HH

#include <string>

template <class VertexBasis, class CellBasis> class MyVTKWriter {
  CellBasis const &cellBasis;
  VertexBasis const &vertexBasis;
  std::string const prefix;

public:
  MyVTKWriter(CellBasis const &cellBasis, VertexBasis const &vertexBasis,
              std::string prefix);

  template <class Vector, class ScalarVector>
  void write(size_t record, Vector const &u, Vector const &v,
             ScalarVector const &alpha, ScalarVector const &stress) const;

  void writeGrid() const;
};
#endif
