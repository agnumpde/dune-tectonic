#ifndef SRC_SPATIAL_SOLVING_FIXEDPOINTITERATOR_HH
#define SRC_SPATIAL_SOLVING_FIXEDPOINTITERATOR_HH

#include <memory>

#include <dune/common/parametertree.hh>

#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/solvers/solver.hh>

struct FixedPointIterationCounter {
  size_t iterations = 0;
  size_t multigridIterations = 0;

  void operator+=(FixedPointIterationCounter const &other);
};

std::ostream &operator<<(std::ostream &stream,
                         FixedPointIterationCounter const &fpic);

template <class Factory, class Updaters, class ErrorNorm>
class FixedPointIterator {
  using ScalarVector = typename Updaters::StateUpdater::ScalarVector;
  using Vector = typename Factory::Vector;
  using Matrix = typename Factory::Matrix;
  using ConvexProblem = typename Factory::ConvexProblem;
  using BlockProblem = typename Factory::BlockProblem;
  using Nonlinearity = typename ConvexProblem::NonlinearityType;

public:
  FixedPointIterator(Factory &factory, Dune::ParameterTree const &parset,
                     std::shared_ptr<Nonlinearity> globalFriction,
                     ErrorNorm const &errorNorm_);

  FixedPointIterationCounter run(Updaters updaters,
                                 Matrix const &velocityMatrix,
                                 Vector const &velocityRHS,
                                 Vector &velocityIterate);

private:
  std::shared_ptr<typename Factory::Step> step_;
  Dune::ParameterTree const &parset_;
  std::shared_ptr<Nonlinearity> globalFriction_;

  size_t fixedPointMaxIterations_;
  double fixedPointTolerance_;
  double lambda_;
  size_t velocityMaxIterations_;
  double velocityTolerance_;
  Solver::VerbosityMode verbosity_;
  ErrorNorm const &errorNorm_;
};
#endif
