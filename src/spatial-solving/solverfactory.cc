#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_IPOPT
#undef HAVE_IPOPT
#endif

#include <dune/fufem/assemblers/transferoperatorassembler.hh>
#include <dune/solvers/solvers/solver.hh>

#include "solverfactory.hh"

template <size_t dim, class BlockProblem, class Grid>
SolverFactory<dim, BlockProblem, Grid>::SolverFactory(
    Dune::ParameterTree const &parset, Grid const &grid,
    Dune::BitSetVector<dim> const &ignoreNodes)
    : baseEnergyNorm(linearBaseSolverStep),
      linearBaseSolver(&linearBaseSolverStep,
                       parset.get<size_t>("linear.maxiumumIterations"),
                       parset.get<double>("linear.tolerance"), &baseEnergyNorm,
                       Solver::QUIET),
      transferOperators(grid.maxLevel()),
      multigridStep(
          std::make_shared<Step>(linearIterationStep, nonlinearSmoother)) {
  // linear iteration step
  linearIterationStep.setMGType(parset.get<int>("linear.cycle"),
                                parset.get<int>("linear.pre"),
                                parset.get<int>("linear.post"));
  linearIterationStep.basesolver_ = &linearBaseSolver;
  linearIterationStep.setSmoother(&linearPresmoother, &linearPostsmoother);

  // transfer operators
  for (auto &&x : transferOperators)
    x = new CompressedMultigridTransfer<Vector>;
  TransferOperatorAssembler<Grid>(grid)
      .assembleOperatorPointerHierarchy(transferOperators);
  linearIterationStep.setTransferOperators(transferOperators);

  // tnnmg iteration step
  multigridStep->setSmoothingSteps(parset.get<int>("main.pre"),
                                   parset.get<int>("main.multi"),
                                   parset.get<int>("main.post"));
  multigridStep->ignoreNodes_ = &ignoreNodes;
}

template <size_t dim, class BlockProblem, class Grid>
SolverFactory<dim, BlockProblem, Grid>::~SolverFactory() {
  for (auto &&x : transferOperators)
    delete x;
}

template <size_t dim, class BlockProblem, class Grid>
auto SolverFactory<dim, BlockProblem, Grid>::getStep()
    -> std::shared_ptr<Step> {
  return multigridStep;
}

#include "solverfactory_tmpl.cc"
