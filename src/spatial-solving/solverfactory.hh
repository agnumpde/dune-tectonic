#ifndef SRC_SPATIAL_SOLVING_SOLVERFACTORY_HH
#define SRC_SPATIAL_SOLVING_SOLVERFACTORY_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/tnnmg/iterationsteps/genericnonlineargs.hh>
#include <dune/tnnmg/iterationsteps/tnnmgstep.hh>

template <size_t dim, class BlockProblemTEMPLATE, class Grid>
class SolverFactory {
public:
  using BlockProblem = BlockProblemTEMPLATE;
  using ConvexProblem = typename BlockProblem::ConvexProblemType;
  using Vector = typename BlockProblem::VectorType;
  using Matrix = typename BlockProblem::MatrixType;

private:
  using NonlinearSmoother = GenericNonlinearGS<BlockProblem>;

public:
  using Step =
      TruncatedNonsmoothNewtonMultigrid<BlockProblem, NonlinearSmoother>;

  SolverFactory(Dune::ParameterTree const &parset, Grid const &grid,
                Dune::BitSetVector<dim> const &ignoreNodes);

  ~SolverFactory();

  std::shared_ptr<Step> getStep();

private:
  TruncatedBlockGSStep<Matrix, Vector> linearBaseSolverStep;
  EnergyNorm<Matrix, Vector> baseEnergyNorm;
  LoopSolver<Vector> linearBaseSolver;
  TruncatedBlockGSStep<Matrix, Vector> linearPresmoother;
  TruncatedBlockGSStep<Matrix, Vector> linearPostsmoother;
  MultigridStep<Matrix, Vector> linearIterationStep;
  std::vector<CompressedMultigridTransfer<Vector> *> transferOperators;
  NonlinearSmoother nonlinearSmoother;
  std::shared_ptr<Step> multigridStep;
};
#endif
