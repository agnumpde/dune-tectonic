#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

#include <dune/common/function.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/tnnmg/problem-classes/convexproblem.hh>

#include <dune/tectonic/globalfriction.hh>
#include <dune/tectonic/myblockproblem.hh>

#include "../time-stepping/rate/rateupdater.hh"
#include "../time-stepping/state/stateupdater.hh"
#include "../time-stepping/updaters.hh"
#include "solverfactory.hh"

using Function = Dune::VirtualFunction<double, double>;
using Factory = SolverFactory<
    MY_DIM,
    MyBlockProblem<ConvexProblem<GlobalFriction<Matrix, Vector>, Matrix>>,
    Grid>;
using MyStateUpdater = StateUpdater<ScalarVector, Vector>;
using MyRateUpdater = RateUpdater<Vector, Matrix, Function, MY_DIM>;
using MyUpdaters = Updaters<MyRateUpdater, MyStateUpdater>;

using ErrorNorm = EnergyNorm<ScalarMatrix, ScalarVector>;

template class FixedPointIterator<Factory, MyUpdaters, ErrorNorm>;
