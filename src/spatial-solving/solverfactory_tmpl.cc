#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

#include <dune/tnnmg/nonlinearities/zerononlinearity.hh>
#include <dune/tnnmg/problem-classes/blocknonlineartnnmgproblem.hh>
#include <dune/tnnmg/problem-classes/convexproblem.hh>

#include <dune/tectonic/globalfriction.hh>
#include <dune/tectonic/myblockproblem.hh>

template class SolverFactory<
    MY_DIM,
    MyBlockProblem<ConvexProblem<GlobalFriction<Matrix, Vector>, Matrix>>,
    Grid>;
template class SolverFactory<
    MY_DIM, BlockNonlinearTNNMGProblem<ConvexProblem<
                ZeroNonlinearity<LocalVector, LocalMatrix>, Matrix>>,
    Grid>;
