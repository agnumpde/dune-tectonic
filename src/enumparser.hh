#ifndef SRC_ENUMPARSER_HH
#define SRC_ENUMPARSER_HH

// Copyright Carsten Graeser 2012

#include <type_traits>

#include <dune/solvers/solvers/solver.hh>

#include "enums.hh"

template <class Enum> struct StringToEnum : public Dune::NotImplemented {};

template <> struct StringToEnum<Config::FrictionModel> {
  static Config::FrictionModel convert(std::string const &s);
};

template <> struct StringToEnum<Config::stateModel> {
  static Config::stateModel convert(std::string const &s);
};

template <> struct StringToEnum<Config::scheme> {
  static Config::scheme convert(std::string const &s);
};

template <> struct StringToEnum<Config::PatchType> {
  static Config::PatchType convert(std::string const &s);
};

template <class Enum>
typename std::enable_if<
    !std::is_base_of<Dune::NotImplemented, StringToEnum<Enum>>::value,
    std::istream &>::type
operator>>(std::istream &lhs, Enum &e);
#endif
