#ifndef SRC_TOBOOL_HH
#define SRC_TOBOOL_HH

#include <dune/common/bitsetvector.hh>

template <class Alloc>
bool toBool(Dune::BitSetVectorConstReference<1, Alloc> x) {
  return x[0];
}
#endif
