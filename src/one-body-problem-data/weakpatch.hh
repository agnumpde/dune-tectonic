#ifndef SRC_ONE_BODY_PROBLEM_DATA_WEAKPATCH_HH
#define SRC_ONE_BODY_PROBLEM_DATA_WEAKPATCH_HH

template <class LocalVector>
ConvexPolyhedron<LocalVector> getWeakPatch(Dune::ParameterTree const &parset) {
  ConvexPolyhedron<LocalVector> weakPatch;
#if MY_DIM == 3
  weakPatch.vertices.resize(4);
  weakPatch.vertices[0] = weakPatch.vertices[2] = MyGeometry::X;
  weakPatch.vertices[1] = weakPatch.vertices[3] = MyGeometry::Y;
  for (size_t k = 0; k < 2; ++k) {
    weakPatch.vertices[k][2] = -MyGeometry::depth / 2.0;
    weakPatch.vertices[k + 2][2] = MyGeometry::depth / 2.0;
  }
  switch (parset.get<Config::PatchType>("patchType")) {
    case Config::Rectangular:
      break;
    case Config::Trapezoidal:
      weakPatch.vertices[1][0] += 0.05 * MyGeometry::lengthScale;
      weakPatch.vertices[3][0] -= 0.05 * MyGeometry::lengthScale;
      break;
    default:
      assert(false);
  }
#else
  weakPatch.vertices.resize(2);
  weakPatch.vertices[0] = MyGeometry::X;
  weakPatch.vertices[1] = MyGeometry::Y;
#endif
  return weakPatch;
};
#endif
