#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "../explicitgrid.hh"
#include "../explicitvectors.hh"

template class GridConstructor<Grid>;

template struct MyFaces<GridView>;

template MyFaces<GridView> GridConstructor<Grid>::constructFaces(
    GridView const &gridView);

template void refine<Grid, LocalVector>(
    Grid &grid, ConvexPolyhedron<LocalVector> const &weakPatch,
    double smallestDiameter);
