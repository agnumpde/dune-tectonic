#ifndef SRC_ONE_BODY_PROBLEM_DATA_BC_HH
#define SRC_ONE_BODY_PROBLEM_DATA_BC_HH

class VelocityDirichletCondition
    : public Dune::VirtualFunction<double, double> {
  void evaluate(double const &relativeTime, double &y) const {
    // Assumed to vanish at time zero
    double const finalVelocity = -5e-5;
    y = (relativeTime <= 0.1)
            ? finalVelocity * (1.0 - std::cos(relativeTime * M_PI / 0.1)) / 2.0
            : finalVelocity;
  }
};

class NeumannCondition : public Dune::VirtualFunction<double, double> {
  void evaluate(double const &relativeTime, double &y) const { y = 0.0; }
};
#endif
