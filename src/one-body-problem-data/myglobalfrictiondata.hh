#ifndef SRC_ONE_BODY_PROBLEM_DATA_MYGLOBALFRICTIONDATA_HH
#define SRC_ONE_BODY_PROBLEM_DATA_MYGLOBALFRICTIONDATA_HH

#include <dune/common/function.hh>

#include <dune/tectonic/globalfrictiondata.hh>

#include "patchfunction.hh"

template <class LocalVector>
class MyGlobalFrictionData : public GlobalFrictionData<LocalVector::dimension> {
private:
  using typename GlobalFrictionData<LocalVector::dimension>::VirtualFunction;

public:
  MyGlobalFrictionData(Dune::ParameterTree const &parset,
                       ConvexPolyhedron<LocalVector> const &segment)
      : C_(parset.get<double>("C")),
        L_(parset.get<double>("L")),
        V0_(parset.get<double>("V0")),
        a_(parset.get<double>("strengthening.a"),
           parset.get<double>("weakening.a"), segment),
        b_(parset.get<double>("strengthening.b"),
           parset.get<double>("weakening.b"), segment),
        mu0_(parset.get<double>("mu0")) {}

  double const &C() const override { return C_; }
  double const &L() const override { return L_; }
  double const &V0() const override { return V0_; }
  VirtualFunction const &a() const override { return a_; }
  VirtualFunction const &b() const override { return b_; }
  double const &mu0() const override { return mu0_; }

private:
  double const C_;
  double const L_;
  double const V0_;
  PatchFunction const a_;
  PatchFunction const b_;
  double const mu0_;
};
#endif
