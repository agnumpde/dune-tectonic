#ifndef SRC_ONE_BODY_PROBLEM_DATA_MYGRID_HH
#define SRC_ONE_BODY_PROBLEM_DATA_MYGRID_HH

#include <dune/common/fmatrix.hh>
#include <dune/grid/common/gridfactory.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/geometry/convexpolyhedron.hh>

#include "mygeometry.hh"

template <class GridView> struct MyFaces {
  BoundaryPatch<GridView> lower;
  BoundaryPatch<GridView> right;
  BoundaryPatch<GridView> upper;

#if MY_DIM == 3
  BoundaryPatch<GridView> front;
  BoundaryPatch<GridView> back;
#endif

  MyFaces(GridView const &gridView);

private:
  bool isClose(double a, double b) {
    return std::abs(a - b) < 1e-14 * MyGeometry::lengthScale;
  };

  bool isClose2(double a, double b) {
    return std::abs(a - b) <
           1e-14 * MyGeometry::lengthScale * MyGeometry::lengthScale;
  };

  template <class Vector>
  bool xyBoxed(Vector const &v1, Vector const &v2, Vector const &x);

  template <class Vector>
  bool xyCollinear(Vector const &a, Vector const &b, Vector const &c);

  template <class Vector>
  bool xyBetween(Vector const &v1, Vector const &v2, Vector const &x);
};

class SimplexManager {
public:
  using SimplexList = std::vector<std::vector<unsigned int>>;

#if MY_DIM == 3
  SimplexManager(unsigned int shift);
#endif

  void addFromVerticesFBB(unsigned int U, unsigned int V, unsigned int W);
  void addFromVerticesFFB(unsigned int U, unsigned int V, unsigned int W);

  SimplexList const &getSimplices();

private:
  SimplexList simplices_;

#if MY_DIM == 3
  unsigned int const shift_;
#endif
};

template <class Grid> class GridConstructor {
public:
  GridConstructor();

  std::shared_ptr<Grid> getGrid();

  template <class GridView>
  MyFaces<GridView> constructFaces(GridView const &gridView);

private:
  Dune::GridFactory<Grid> gridFactory;
};

double computeAdmissibleDiameter(double distance, double smallestDiameter);

template <class Grid, class LocalVector>
void refine(Grid &grid, ConvexPolyhedron<LocalVector> const &weakPatch,
            double smallestDiameter);

#endif
