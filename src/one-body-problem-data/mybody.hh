#ifndef SRC_ONE_BODY_PROBLEM_DATA_MYBODY_HH
#define SRC_ONE_BODY_PROBLEM_DATA_MYBODY_HH

#include <dune/common/fvector.hh>

#include <dune/fufem/functions/constantfunction.hh>

#include <dune/tectonic/body.hh>
#include <dune/tectonic/gravity.hh>

#include "mygeometry.hh"
#include "segmented-function.hh"

template <int dimension> class MyBody : public Body<dimension> {
  using typename Body<dimension>::ScalarFunction;
  using typename Body<dimension>::VectorField;

public:
  MyBody(Dune::ParameterTree const &parset)
      : poissonRatio_(parset.get<double>("body.poissonRatio")),
        youngModulus_(3.0 * parset.get<double>("body.bulkModulus") *
                      (1.0 - 2.0 * poissonRatio_)),
        shearViscosityField_(
            parset.get<double>("body.elastic.shearViscosity"),
            parset.get<double>("body.viscoelastic.shearViscosity")),
        bulkViscosityField_(
            parset.get<double>("body.elastic.bulkViscosity"),
            parset.get<double>("body.viscoelastic.bulkViscosity")),
        densityField_(parset.get<double>("body.elastic.density"),
                      parset.get<double>("body.viscoelastic.density")),
        gravityField_(densityField_, MyGeometry::zenith,
                      parset.get<double>("gravity")) {}

  double getPoissonRatio() const override { return poissonRatio_; }
  double getYoungModulus() const override { return youngModulus_; }
  ScalarFunction const &getShearViscosityField() const override {
    return shearViscosityField_;
  }
  ScalarFunction const &getBulkViscosityField() const override {
    return bulkViscosityField_;
  }
  ScalarFunction const &getDensityField() const override {
    return densityField_;
  }
  VectorField const &getGravityField() const override { return gravityField_; }

private:
  double const poissonRatio_;
  double const youngModulus_;
  SegmentedFunction const shearViscosityField_;
  SegmentedFunction const bulkViscosityField_;
  SegmentedFunction const densityField_;
  Gravity<dimension> const gravityField_;
};
#endif
