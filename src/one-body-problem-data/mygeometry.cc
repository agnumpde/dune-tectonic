#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fstream>

#ifdef HAVE_CAIROMM
#include <cairomm/context.h>
#include <cairomm/fontface.h>
#include <cairomm/surface.h>
#endif

#include "mygeometry.hh"

void MyGeometry::write() {
  std::fstream writer("geometry", std::fstream::out);
  writer << "A = " << A << std::endl;
  writer << "B = " << B << std::endl;
  writer << "C = " << C << std::endl;
  writer << "Y = " << Y << std::endl;
  writer << "X = " << X << std::endl;
  writer << "Z = " << Z << std::endl;
  writer << "U = " << U << std::endl;
  writer << "K = " << K << std::endl;
  writer << "M = " << M << std::endl;
  writer << "G = " << G << std::endl;
  writer << "H = " << H << std::endl;
  writer << "J = " << J << std::endl;
  writer << "I = " << I << std::endl;
  writer << "zenith = " << zenith << std::endl;
}

void MyGeometry::render() {
#ifdef HAVE_CAIROMM
  std::string const filename = "geometry.png";
  double const width = 600;
  double const height = 400;
  double const widthScale = 400;
  double const heightScale = 400;

  auto surface =
      Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
  auto cr = Cairo::Context::create(surface);

  auto const setRGBColor = [&](int colour) {
    cr->set_source_rgb(((colour & 0xFF0000) >> 16) / 255.0,
                       ((colour & 0x00FF00) >> 8) / 255.0,
                       ((colour & 0x0000FF) >> 0) / 255.0);
  };
  auto const moveTo = [&](LocalVector2D const &v) { cr->move_to(v[0], -v[1]); };
  auto const lineTo = [&](LocalVector2D const &v) { cr->line_to(v[0], -v[1]); };

  cr->scale(widthScale, heightScale);
  cr->translate(0.1, 0.1);
  cr->set_line_width(0.0025);

  // triangle
  {
    moveTo(reference::A);
    lineTo(reference::B);
    lineTo(reference::C);
    cr->close_path();
    cr->stroke();
  }

  // dashed lines
  {
    cr->save();
    std::vector<double> dashPattern = { 0.005 };
    cr->set_dash(dashPattern, 0);
    moveTo(reference::Z);
    lineTo(reference::Y);
    moveTo(reference::U);
    lineTo(reference::X);
    cr->stroke();
    cr->restore();
  }

  // fill viscoelastic region
  {
    cr->save();
    setRGBColor(0x0097E0);
    moveTo(reference::B);
    lineTo(reference::K);
    lineTo(reference::M);
    cr->fill();
    cr->restore();
  }

  // mark weakening region
  {
    cr->save();
    setRGBColor(0x7AD3FF);
    cr->set_line_width(0.005);
    moveTo(reference::X);
    lineTo(reference::Y);
    cr->stroke();
    cr->restore();
  }

  // mark points
  {
    auto const drawCircle = [&](LocalVector2D const &v) {
      cr->arc(v[0], -v[1], 0.0075, -M_PI, M_PI); // x,y,radius,angle1,angle2
      cr->fill();
    };

    cr->save();
    setRGBColor(0x002F47);
    drawCircle(reference::A);
    drawCircle(reference::B);
    drawCircle(reference::C);
    drawCircle(reference::Y);
    drawCircle(reference::X);
    drawCircle(reference::Z);
    drawCircle(reference::U);
    drawCircle(reference::K);
    drawCircle(reference::M);
    drawCircle(reference::G);
    drawCircle(reference::H);
    drawCircle(reference::J);
    drawCircle(reference::I);
    cr->restore();
  }

  // labels
  {
    auto const label = [&](LocalVector2D const &v, std::string l) {
      moveTo(v);
      cr->rel_move_to(0.005, -0.02);
      cr->show_text(l);
    };
    auto font = Cairo::ToyFontFace::create(
        "monospace", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);

    cr->save();
    cr->set_font_face(font);
    cr->set_font_size(0.03);

    label(reference::A, "A");
    label(reference::B, "B");
    label(reference::C, "C");
    label(reference::K, "K");
    label(reference::M, "M");
    label(reference::U, "U");
    label(reference::X, "X");
    label(reference::Y, "Y");
    label(reference::Z, "Z");
    label(reference::G, "G");
    label(reference::H, "H");
    label(reference::J, "J");
    label(reference::I, "I");
    cr->restore();
  }

  surface->write_to_png(filename);
#endif
}
