#ifndef SRC_ONE_BODY_PROBLEM_DATA_SEGMENTED_FUNCTION_HH
#define SRC_ONE_BODY_PROBLEM_DATA_SEGMENTED_FUNCTION_HH

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include "mygeometry.hh"

class SegmentedFunction
    : public Dune::VirtualFunction<Dune::FieldVector<double, MY_DIM>,
                                   Dune::FieldVector<double, 1>> {
private:
  bool liesBelow(Dune::FieldVector<double, MY_DIM> const &x,
                 Dune::FieldVector<double, MY_DIM> const &y,
                 Dune::FieldVector<double, MY_DIM> const &z) const {
    return x[1] + (z[0] - x[0]) * (y[1] - x[1]) / (y[0] - x[0]) >= z[1];
  };
  bool insideRegion2(Dune::FieldVector<double, MY_DIM> const &z) const {
    return liesBelow(MyGeometry::K, MyGeometry::M, z);
  };

  double const _v1;
  double const _v2;

public:
  SegmentedFunction(double v1, double v2) : _v1(v1), _v2(v2) {}

  void evaluate(Dune::FieldVector<double, MY_DIM> const &x,
                Dune::FieldVector<double, 1> &y) const {
    y = insideRegion2(x) ? _v2 : _v1;
  }
};
#endif
