#ifndef SRC_ONE_BODY_PROBLEM_DATA_PATCHFUNCTION_HH
#define SRC_ONE_BODY_PROBLEM_DATA_PATCHFUNCTION_HH

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/fufem/geometry/polyhedrondistance.hh>

class PatchFunction
    : public Dune::VirtualFunction<Dune::FieldVector<double, MY_DIM>,
                                   Dune::FieldVector<double, 1>> {
private:
  using Polyhedron = ConvexPolyhedron<Dune::FieldVector<double, MY_DIM>>;

  double const v1_;
  double const v2_;
  Polyhedron const &segment_;

public:
  PatchFunction(double v1, double v2, Polyhedron const &segment)
      : v1_(v1), v2_(v2), segment_(segment) {}

  void evaluate(Dune::FieldVector<double, MY_DIM> const &x,
                Dune::FieldVector<double, 1> &y) const {
    y = distance(x, segment_, 1e-6 * MyGeometry::lengthScale) <= 1e-5 ? v2_
                                                                      : v1_;
  }
};

#endif
