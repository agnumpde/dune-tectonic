#ifndef SRC_MIDPOINT_HH
#define SRC_MIDPOINT_HH

#include <dune/solvers/common/arithmetic.hh>

template <class Vector> Vector midPoint(Vector const &x, Vector const &y) {
  Vector ret(0);
  Arithmetic::addProduct(ret, 0.5, x);
  Arithmetic::addProduct(ret, 0.5, y);
  return ret;
}
#endif
