#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/fufem/functions/vtkbasisgridfunction.hh>

#include "vtk.hh"

template <class VertexBasis, class CellBasis>
MyVTKWriter<VertexBasis, CellBasis>::MyVTKWriter(
    CellBasis const &_cellBasis, VertexBasis const &_vertexBasis,
    std::string _prefix)
    : cellBasis(_cellBasis), vertexBasis(_vertexBasis), prefix(_prefix) {}

template <class VertexBasis, class CellBasis>
template <class Vector, class ScalarVector>
void MyVTKWriter<VertexBasis, CellBasis>::write(
    size_t record, Vector const &u, Vector const &v, ScalarVector const &alpha,
    ScalarVector const &stress) const {
  Dune::VTKWriter<typename VertexBasis::GridView> writer(
      vertexBasis.getGridView());

  auto const displacementPointer =
      std::make_shared<VTKBasisGridFunction<VertexBasis, Vector> const>(
          vertexBasis, u, "displacement");
  writer.addVertexData(displacementPointer);

  auto const velocityPointer =
      std::make_shared<VTKBasisGridFunction<VertexBasis, Vector> const>(
          vertexBasis, v, "velocity");
  writer.addVertexData(velocityPointer);

  auto const AlphaPointer =
      std::make_shared<VTKBasisGridFunction<VertexBasis, ScalarVector> const>(
          vertexBasis, alpha, "Alpha");
  writer.addVertexData(AlphaPointer);

  auto const stressPointer =
      std::make_shared<VTKBasisGridFunction<CellBasis, ScalarVector> const>(
          cellBasis, stress, "stress");
  writer.addCellData(stressPointer);

  std::string const filename = prefix + std::to_string(record);
  writer.write(filename.c_str(), Dune::VTK::appendedraw);
}

template <class VertexBasis, class CellBasis>
void MyVTKWriter<VertexBasis, CellBasis>::writeGrid() const {
  Dune::VTKWriter<typename VertexBasis::GridView> writer(
      vertexBasis.getGridView());

  std::string const filename = prefix + "_grid";
  writer.write(filename.c_str(), Dune::VTK::appendedraw);
}

#include "vtk_tmpl.cc"
