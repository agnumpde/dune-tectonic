#ifndef MY_DIM
#error MY_DIM unset
#endif

#include "explicitgrid.hh"
#include "explicitvectors.hh"

#include <dune/fufem/functionspacebases/p0basis.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>

using MyP0Basis = P0Basis<GridView, double>;
using P1Basis = P1NodalBasis<GridView, double>;

template class MyVTKWriter<P1Basis, MyP0Basis>;

template void MyVTKWriter<P1Basis, MyP0Basis>::write<Vector, ScalarVector>(
    size_t record, Vector const &u, Vector const &v, ScalarVector const &alpha,
    ScalarVector const &stress) const;
