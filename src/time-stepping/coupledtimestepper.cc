#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "coupledtimestepper.hh"

template <class Factory, class Updaters, class ErrorNorm>
CoupledTimeStepper<Factory, Updaters, ErrorNorm>::CoupledTimeStepper(
    double finalTime, Factory &factory, Dune::ParameterTree const &parset,
    std::shared_ptr<Nonlinearity> globalFriction, Updaters updaters,
    ErrorNorm const &errorNorm,
    std::function<void(double, Vector &)> externalForces)
    : finalTime_(finalTime),
      factory_(factory),
      parset_(parset),
      globalFriction_(globalFriction),
      updaters_(updaters),
      externalForces_(externalForces),
      errorNorm_(errorNorm) {}

template <class Factory, class Updaters, class ErrorNorm>
FixedPointIterationCounter
CoupledTimeStepper<Factory, Updaters, ErrorNorm>::step(double relativeTime,
                                                       double relativeTau) {
  updaters_.state_->nextTimeStep();
  updaters_.rate_->nextTimeStep();

  auto const newRelativeTime = relativeTime + relativeTau;
  Vector ell;
  externalForces_(newRelativeTime, ell);

  Matrix velocityMatrix;
  Vector velocityRHS;
  Vector velocityIterate;

  auto const tau = relativeTau * finalTime_;
  updaters_.state_->setup(tau);
  updaters_.rate_->setup(ell, tau, newRelativeTime, velocityRHS,
                         velocityIterate, velocityMatrix);
  FixedPointIterator<Factory, Updaters, ErrorNorm> fixedPointIterator(
      factory_, parset_, globalFriction_, errorNorm_);
  auto const iterations = fixedPointIterator.run(updaters_, velocityMatrix,
                                                 velocityRHS, velocityIterate);
  return iterations;
}

#include "coupledtimestepper_tmpl.cc"
