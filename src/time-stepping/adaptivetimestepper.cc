#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "adaptivetimestepper.hh"

void IterationRegister::registerCount(FixedPointIterationCounter count) {
  totalCount += count;
}

void IterationRegister::registerFinalCount(FixedPointIterationCounter count) {
  finalCount = count;
}

void IterationRegister::reset() {
  totalCount = FixedPointIterationCounter();
  finalCount = FixedPointIterationCounter();
}

template <class Factory, class Updaters, class ErrorNorm>
AdaptiveTimeStepper<Factory, Updaters, ErrorNorm>::AdaptiveTimeStepper(
    Factory &factory, Dune::ParameterTree const &parset,
    std::shared_ptr<Nonlinearity> globalFriction, Updaters &current,
    double relativeTime, double relativeTau,
    std::function<void(double, Vector &)> externalForces,
    ErrorNorm const &errorNorm,
    std::function<bool(Updaters &, Updaters &)> mustRefine)
    : relativeTime_(relativeTime),
      relativeTau_(relativeTau),
      finalTime_(parset.get<double>("problem.finalTime")),
      factory_(factory),
      parset_(parset),
      globalFriction_(globalFriction),
      current_(current),
      R1_(),
      externalForces_(externalForces),
      mustRefine_(mustRefine),
      errorNorm_(errorNorm) {}

template <class Factory, class Updaters, class ErrorNorm>
bool AdaptiveTimeStepper<Factory, Updaters, ErrorNorm>::reachedEnd() {
  return relativeTime_ >= 1.0;
}

template <class Factory, class Updaters, class ErrorNorm>
IterationRegister AdaptiveTimeStepper<Factory, Updaters, ErrorNorm>::advance() {
  /*
    |     C     | We check here if making the step R1 of size tau is a
    |  R1 | R2  | good idea. To check if we can coarsen, we compare
    |F1|F2|     | the result of (R1+R2) with C, i.e. two steps of size
                  tau with one of size 2*tau. To check if we need to
    refine, we compare the result of (F1+F2) with R1, i.e. two steps
    of size tau/2 with one of size tau. The method makes multiple
    coarsening/refining attempts, with coarsening coming first. */
  if (R1_.updaters == Updaters())
    R1_ = step(current_, relativeTime_, relativeTau_);

  bool didCoarsen = false;
  iterationRegister_.reset();
  UpdatersWithCount R2;
  UpdatersWithCount C;
  while (relativeTime_ + relativeTau_ <= 1.0) {
    R2 = step(R1_.updaters, relativeTime_ + relativeTau_, relativeTau_);
    C = step(current_, relativeTime_, 2 * relativeTau_);
    if (mustRefine_(R2.updaters, C.updaters))
      break;

    didCoarsen = true;
    relativeTau_ *= 2;
    R1_ = C;
  }
  UpdatersWithCount F1;
  UpdatersWithCount F2;
  if (!didCoarsen) {
    while (true) {
      F1 = step(current_, relativeTime_, relativeTau_ / 2.0);
      F2 = step(F1.updaters, relativeTime_ + relativeTau_ / 2.0,
                relativeTau_ / 2.0);
      if (!mustRefine_(F2.updaters, R1_.updaters))
        break;

      relativeTau_ /= 2.0;
      R1_ = F1;
      R2 = F2;
    }
  }

  iterationRegister_.registerFinalCount(R1_.count);
  relativeTime_ += relativeTau_;
  current_ = R1_.updaters;
  R1_ = R2;

  return iterationRegister_;
}

template <class Factory, class Updaters, class ErrorNorm>
typename AdaptiveTimeStepper<Factory, Updaters, ErrorNorm>::UpdatersWithCount
AdaptiveTimeStepper<Factory, Updaters, ErrorNorm>::step(
    Updaters const &oldUpdaters, double rTime, double rTau) {
  UpdatersWithCount newUpdatersAndCount = {oldUpdaters.clone(), {}};
  newUpdatersAndCount.count =
      MyCoupledTimeStepper(finalTime_, factory_, parset_, globalFriction_,
                           newUpdatersAndCount.updaters, errorNorm_,
                           externalForces_)
          .step(rTime, rTau);
  iterationRegister_.registerCount(newUpdatersAndCount.count);
  return newUpdatersAndCount;
}

#include "adaptivetimestepper_tmpl.cc"
