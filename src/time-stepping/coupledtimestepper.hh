#ifndef SRC_TIME_STEPPING_COUPLEDTIMESTEPPER_HH
#define SRC_TIME_STEPPING_COUPLEDTIMESTEPPER_HH

#include <functional>
#include <memory>

#include <dune/common/parametertree.hh>

#include "../spatial-solving/fixedpointiterator.hh"

template <class Factory, class Updaters, class ErrorNorm>
class CoupledTimeStepper {
  using Vector = typename Factory::Vector;
  using Matrix = typename Factory::Matrix;
  using ConvexProblem = typename Factory::ConvexProblem;
  using Nonlinearity = typename ConvexProblem::NonlinearityType;

public:
  CoupledTimeStepper(double finalTime, Factory &factory,
                     Dune::ParameterTree const &parset,
                     std::shared_ptr<Nonlinearity> globalFriction,
                     Updaters updaters, ErrorNorm const &errorNorm,
                     std::function<void(double, Vector &)> externalForces);

  FixedPointIterationCounter step(double relativeTime, double relativeTau);

private:
  double finalTime_;
  Factory &factory_;
  Dune::ParameterTree const &parset_;
  std::shared_ptr<Nonlinearity> globalFriction_;
  Updaters updaters_;
  std::function<void(double, Vector &)> externalForces_;
  ErrorNorm const &errorNorm_;
};
#endif
