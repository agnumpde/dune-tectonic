#ifndef SRC_TIME_STEPPING_RATE_HH
#define SRC_TIME_STEPPING_RATE_HH

#include <memory>

#include "../enums.hh"
#include "rate/rateupdater.hh"

template <class Vector, class Matrix, class Function, int dimension>
std::shared_ptr<RateUpdater<Vector, Matrix, Function, dimension>>
initRateUpdater(Config::scheme scheme,
                Function const &velocityDirichletFunction,
                Dune::BitSetVector<dimension> const &velocityDirichletNodes,
                Matrices<Matrix> const &matrices, Vector const &u_initial,
                Vector const &v_initial, Vector const &a_initial);
#endif
