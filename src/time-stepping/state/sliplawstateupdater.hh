#ifndef SRC_TIME_STEPPING_STATE_SLIPLAWSTATEUPDATER_HH
#define SRC_TIME_STEPPING_STATE_SLIPLAWSTATEUPDATER_HH

#include "stateupdater.hh"

template <class ScalarVector, class Vector>
class SlipLawStateUpdater : public StateUpdater<ScalarVector, Vector> {
public:
  SlipLawStateUpdater(ScalarVector const &_alpha_initial,
                      Dune::BitSetVector<1> const &_nodes, double _L,
                      double _V0);

  void nextTimeStep() override;
  void setup(double _tau) override;
  void solve(Vector const &velocity_field) override;
  void extractAlpha(ScalarVector &) override;

  std::shared_ptr<StateUpdater<ScalarVector, Vector>> clone() const override;

private:
  ScalarVector alpha_o;
  ScalarVector alpha;
  Dune::BitSetVector<1> const &nodes;
  double const L;
  double const V0;
  double tau;
};

#endif
