#include <cmath>

#include "sliplawstateupdater.hh"
#include "../../tobool.hh"

template <class ScalarVector, class Vector>
SlipLawStateUpdater<ScalarVector, Vector>::SlipLawStateUpdater(
    ScalarVector const &_alpha_initial, Dune::BitSetVector<1> const &_nodes,
    double _L, double _V0)
    : alpha(_alpha_initial), nodes(_nodes), L(_L), V0(_V0) {}

template <class ScalarVector, class Vector>
void SlipLawStateUpdater<ScalarVector, Vector>::nextTimeStep() {
  alpha_o = alpha;
}

template <class ScalarVector, class Vector>
void SlipLawStateUpdater<ScalarVector, Vector>::setup(double _tau) {
  tau = _tau;
}

template <class ScalarVector, class Vector>
void SlipLawStateUpdater<ScalarVector, Vector>::solve(
    Vector const &velocity_field) {
  for (size_t i = 0; i < nodes.size(); ++i) {
    if (not toBool(nodes[i]))
      continue;

    double const V = velocity_field[i].two_norm();
    double const mtVoL = -tau * V / L;
    alpha[i] = (V <= 0) ? alpha_o[i] : std::expm1(mtVoL) * std::log(V / V0) +
                                           alpha_o[i] * std::exp(mtVoL);
  }
}

template <class ScalarVector, class Vector>
void SlipLawStateUpdater<ScalarVector, Vector>::extractAlpha(
    ScalarVector &_alpha) {
  _alpha = alpha;
}

template <class ScalarVector, class Vector>
std::shared_ptr<StateUpdater<ScalarVector, Vector>>
SlipLawStateUpdater<ScalarVector, Vector>::clone() const {
  return std::make_shared<SlipLawStateUpdater<ScalarVector, Vector>>(*this);
}
