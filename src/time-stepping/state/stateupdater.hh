#ifndef SRC_TIME_STEPPING_STATE_STATEUPDATER_HH
#define SRC_TIME_STEPPING_STATE_STATEUPDATER_HH

template <class ScalarVectorTEMPLATE, class Vector> class StateUpdater {
public:
  using ScalarVector = ScalarVectorTEMPLATE;

  void virtual nextTimeStep() = 0;
  void virtual setup(double _tau) = 0;
  void virtual solve(Vector const &velocity_field) = 0;
  void virtual extractAlpha(ScalarVector &alpha) = 0;

  std::shared_ptr<StateUpdater<ScalarVector, Vector>> virtual clone() const = 0;
};

#endif
