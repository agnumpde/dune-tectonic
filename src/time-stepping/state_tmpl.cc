#include "../explicitvectors.hh"

template std::shared_ptr<StateUpdater<ScalarVector, Vector>>
initStateUpdater<ScalarVector, Vector>(
    Config::stateModel model, ScalarVector const &alpha_initial,
    Dune::BitSetVector<1> const &frictionalNodes, double L, double V0);
