#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "rate.hh"
#include "rate/backward_euler.hh"
#include "rate/newmark.hh"

template <class Vector, class Matrix, class Function, int dimension>
std::shared_ptr<RateUpdater<Vector, Matrix, Function, dimension>>
initRateUpdater(Config::scheme scheme,
                Function const &velocityDirichletFunction,
                Dune::BitSetVector<dimension> const &velocityDirichletNodes,
                Matrices<Matrix> const &matrices, Vector const &u_initial,
                Vector const &v_initial, Vector const &a_initial) {
  switch (scheme) {
    case Config::Newmark:
      return std::make_shared<Newmark<Vector, Matrix, Function, dimension>>(
          matrices, u_initial, v_initial, a_initial, velocityDirichletNodes,
          velocityDirichletFunction);
    case Config::BackwardEuler:
      return std::make_shared<
          BackwardEuler<Vector, Matrix, Function, dimension>>(
          matrices, u_initial, v_initial, a_initial, velocityDirichletNodes,
          velocityDirichletFunction);
    default:
      assert(false);
  }
}

#include "rate_tmpl.cc"
