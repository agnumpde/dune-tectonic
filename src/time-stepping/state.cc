#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "state.hh"
#include "state/ageinglawstateupdater.cc"
#include "state/sliplawstateupdater.cc"

template <class ScalarVector, class Vector>
std::shared_ptr<StateUpdater<ScalarVector, Vector>> initStateUpdater(
    Config::stateModel model, ScalarVector const &alpha_initial,
    Dune::BitSetVector<1> const &frictionalNodes, double L, double V0) {
  switch (model) {
    case Config::AgeingLaw:
      return std::make_shared<AgeingLawStateUpdater<ScalarVector, Vector>>(
          alpha_initial, frictionalNodes, L, V0);
    case Config::SlipLaw:
      return std::make_shared<SlipLawStateUpdater<ScalarVector, Vector>>(
          alpha_initial, frictionalNodes, L, V0);
    default:
      assert(false);
  }
}

#include "state_tmpl.cc"
