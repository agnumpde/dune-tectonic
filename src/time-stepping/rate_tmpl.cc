#include "../explicitvectors.hh"

#include <dune/common/function.hh>

using Function = Dune::VirtualFunction<double, double>;

template std::shared_ptr<RateUpdater<Vector, Matrix, Function, MY_DIM>>
initRateUpdater<Vector, Matrix, Function, MY_DIM>(
    Config::scheme scheme, Function const &velocityDirichletFunction,
    Dune::BitSetVector<MY_DIM> const &velocityDirichletNodes,
    Matrices<Matrix> const &matrices, Vector const &u_initial,
    Vector const &v_initial, Vector const &a_initial);
