#ifndef SRC_TIME_STEPPING_ADAPTIVETIMESTEPPER_HH
#define SRC_TIME_STEPPING_ADAPTIVETIMESTEPPER_HH

#include <fstream>

#include "coupledtimestepper.hh"

struct IterationRegister {
  void registerCount(FixedPointIterationCounter count);
  void registerFinalCount(FixedPointIterationCounter count);

  void reset();

  FixedPointIterationCounter totalCount;
  FixedPointIterationCounter finalCount;
};

template <class Factory, class Updaters, class ErrorNorm>
class AdaptiveTimeStepper {
  struct UpdatersWithCount {
    Updaters updaters;
    FixedPointIterationCounter count;
  };

  using Vector = typename Factory::Vector;
  using ConvexProblem = typename Factory::ConvexProblem;
  using Nonlinearity = typename ConvexProblem::NonlinearityType;

  using MyCoupledTimeStepper = CoupledTimeStepper<Factory, Updaters, ErrorNorm>;

public:
  AdaptiveTimeStepper(Factory &factory, Dune::ParameterTree const &parset,
                      std::shared_ptr<Nonlinearity> globalFriction,
                      Updaters &current, double relativeTime,
                      double relativeTau,
                      std::function<void(double, Vector &)> externalForces,
                      ErrorNorm const &errorNorm,
                      std::function<bool(Updaters &, Updaters &)> mustRefine);

  bool reachedEnd();
  IterationRegister advance();

  double relativeTime_;
  double relativeTau_;

private:
  UpdatersWithCount step(Updaters const &oldUpdaters, double rTime,
                         double rTau);

  double finalTime_;
  Factory &factory_;
  Dune::ParameterTree const &parset_;
  std::shared_ptr<Nonlinearity> globalFriction_;
  Updaters &current_;
  UpdatersWithCount R1_;
  std::function<void(double, Vector &)> externalForces_;
  std::function<bool(Updaters &, Updaters &)> mustRefine_;
  ErrorNorm const &errorNorm_;

  IterationRegister iterationRegister_;
};
#endif
