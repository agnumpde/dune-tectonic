#ifndef SRC_TIME_STEPPING_RATE_RATEUPDATER_HH
#define SRC_TIME_STEPPING_RATE_RATEUPDATER_HH

#include <memory>

#include <dune/common/bitsetvector.hh>

#include "../../matrices.hh"

template <class Vector, class Matrix, class Function, size_t dim>
class RateUpdater {
protected:
  RateUpdater(Matrices<Matrix> const &_matrices, Vector const &_u_initial,
              Vector const &_v_initial, Vector const &_a_initial,
              Dune::BitSetVector<dim> const &_dirichletNodes,
              Function const &_dirichletFunction);

public:
  void nextTimeStep();
  void virtual setup(Vector const &ell, double _tau, double relativeTime,
                     Vector &rhs, Vector &iterate, Matrix &AB) = 0;

  void virtual postProcess(Vector const &iterate) = 0;
  void extractDisplacement(Vector &displacement) const;
  void extractVelocity(Vector &velocity) const;
  void extractOldVelocity(Vector &velocity) const;
  void extractAcceleration(Vector &acceleration) const;

  std::shared_ptr<RateUpdater<Vector, Matrix, Function, dim>> virtual clone()
      const = 0;

protected:
  Matrices<Matrix> const &matrices;
  Vector u, v, a;
  Dune::BitSetVector<dim> const &dirichletNodes;
  Function const &dirichletFunction;
  double dirichletValue;

  Vector u_o, v_o, a_o;
  double tau;

  bool postProcessCalled = true;
};
#endif
