#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "rateupdater.hh"

template <class Vector, class Matrix, class Function, size_t dim>
RateUpdater<Vector, Matrix, Function, dim>::RateUpdater(
    Matrices<Matrix> const &_matrices, Vector const &_u_initial,
    Vector const &_v_initial, Vector const &_a_initial,
    Dune::BitSetVector<dim> const &_dirichletNodes,
    Function const &_dirichletFunction)
    : matrices(_matrices),
      u(_u_initial),
      v(_v_initial),
      a(_a_initial),
      dirichletNodes(_dirichletNodes),
      dirichletFunction(_dirichletFunction) {}

template <class Vector, class Matrix, class Function, size_t dim>
void RateUpdater<Vector, Matrix, Function, dim>::nextTimeStep() {
  u_o = u;
  v_o = v;
  a_o = a;
  postProcessCalled = false;
}

template <class Vector, class Matrix, class Function, size_t dim>
void RateUpdater<Vector, Matrix, Function, dim>::extractDisplacement(
    Vector &displacement) const {
  if (!postProcessCalled)
    DUNE_THROW(Dune::Exception, "It seems you forgot to call postProcess!");

  displacement = u;
}

template <class Vector, class Matrix, class Function, size_t dim>
void RateUpdater<Vector, Matrix, Function, dim>::extractVelocity(
    Vector &velocity) const {
  if (!postProcessCalled)
    DUNE_THROW(Dune::Exception, "It seems you forgot to call postProcess!");

  velocity = v;
}

template <class Vector, class Matrix, class Function, size_t dim>
void RateUpdater<Vector, Matrix, Function, dim>::extractOldVelocity(
    Vector &oldVelocity) const {
  oldVelocity = v_o;
}

template <class Vector, class Matrix, class Function, size_t dim>
void RateUpdater<Vector, Matrix, Function, dim>::extractAcceleration(
    Vector &acceleration) const {
  if (!postProcessCalled)
    DUNE_THROW(Dune::Exception, "It seems you forgot to call postProcess!");

  acceleration = a;
}

#include "backward_euler.cc"
#include "newmark.cc"
#include "rateupdater_tmpl.cc"
