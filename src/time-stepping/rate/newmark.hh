#ifndef SRC_TIME_STEPPING_RATE_NEWMARK_HH
#define SRC_TIME_STEPPING_RATE_NEWMARK_HH

template <class Vector, class Matrix, class Function, size_t dim>
class Newmark : public RateUpdater<Vector, Matrix, Function, dim> {
public:
  Newmark(Matrices<Matrix> const &_matrices, Vector const &_u_initial,
          Vector const &_v_initial, Vector const &_a_initial,
          Dune::BitSetVector<dim> const &_dirichletNodes,
          Function const &_dirichletFunction);

  void setup(Vector const &, double, double, Vector &, Vector &,
             Matrix &) override;
  void postProcess(Vector const &) override;

  std::shared_ptr<RateUpdater<Vector, Matrix, Function, dim>> clone()
      const override;
};
#endif
