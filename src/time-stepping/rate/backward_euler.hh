#ifndef SRC_TIME_STEPPING_RATE_BACKWARD_EULER_HH
#define SRC_TIME_STEPPING_RATE_BACKWARD_EULER_HH

template <class Vector, class Matrix, class Function, size_t dim>
class BackwardEuler : public RateUpdater<Vector, Matrix, Function, dim> {
public:
  BackwardEuler(Matrices<Matrix> const &_matrices, Vector const &_u_initial,
                Vector const &_v_initial, Vector const &_a_initial,
                Dune::BitSetVector<dim> const &_dirichletNodes,
                Function const &_dirichletFunction);

  void setup(Vector const &, double, double, Vector &, Vector &,
             Matrix &) override;
  void postProcess(Vector const &) override;

  std::shared_ptr<RateUpdater<Vector, Matrix, Function, dim>> clone()
      const override;

private:
};
#endif
