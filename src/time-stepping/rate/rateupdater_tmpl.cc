#ifndef MY_DIM
#error MY_DIM unset
#endif

#include <dune/common/function.hh>

#include "../../explicitvectors.hh"

using Function = Dune::VirtualFunction<double, double>;

template class RateUpdater<Vector, Matrix, Function, MY_DIM>;
template class Newmark<Vector, Matrix, Function, MY_DIM>;
template class BackwardEuler<Vector, Matrix, Function, MY_DIM>;
