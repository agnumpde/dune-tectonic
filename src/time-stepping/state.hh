#ifndef SRC_TIME_STEPPING_STATE_HH
#define SRC_TIME_STEPPING_STATE_HH

#include <memory>

#include <dune/common/bitsetvector.hh>

#include "../enums.hh"
#include "state/ageinglawstateupdater.hh"
#include "state/sliplawstateupdater.hh"
#include "state/stateupdater.hh"

template <class ScalarVector, class Vector>
std::shared_ptr<StateUpdater<ScalarVector, Vector>> initStateUpdater(
    Config::stateModel model, ScalarVector const &alpha_initial,
    Dune::BitSetVector<1> const &frictionalNodes, double L, double V0);

#endif
