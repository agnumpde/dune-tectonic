#ifndef SRC_EXPLICITVECTORS_HH
#define SRC_EXPLICITVECTORS_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

using LocalVector = Dune::FieldVector<double, MY_DIM>;
using LocalMatrix = Dune::FieldMatrix<double, MY_DIM, MY_DIM>;
using Vector = Dune::BlockVector<LocalVector>;
using Matrix = Dune::BCRSMatrix<LocalMatrix>;
using ScalarVector = Dune::BlockVector<Dune::FieldVector<double, 1>>;
using ScalarMatrix = Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>>;
#endif
