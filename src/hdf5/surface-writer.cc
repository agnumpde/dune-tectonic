#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "restrict.hh"
#include "surface-writer.hh"

template <class ProgramState, class GridView>
SurfaceWriter<ProgramState, GridView>::SurfaceWriter(
    HDF5::Grouplike &file, Vector const &vertexCoordinates, Patch const &surface)
    : group_(file, "surface"),
      surface_(surface),
      surfaceDisplacementWriter_(group_, "displacement", surface.numVertices(),
                                 Vector::block_type::dimension),
      surfaceVelocityWriter_(group_, "velocity", surface.numVertices(),
                             Vector::block_type::dimension) {
  auto const surfaceCoordinates = restrictToSurface(vertexCoordinates, surface);
  HDF5::SingletonWriter<2> surfaceCoordinateWriter(group_, "coordinates",
                                                 surfaceCoordinates.size(),
                                                 Vector::block_type::dimension);
  setEntry(surfaceCoordinateWriter, surfaceCoordinates);
}

template <class ProgramState, class GridView>
void SurfaceWriter<ProgramState, GridView>::write(
    ProgramState const &programState) {
  auto const surfaceDisplacements = restrictToSurface(programState.u, surface_);
  addEntry(surfaceDisplacementWriter_, programState.timeStep,
           surfaceDisplacements);

  auto const surfaceVelocities = restrictToSurface(programState.v, surface_);
  addEntry(surfaceVelocityWriter_, programState.timeStep, surfaceVelocities);
}

#include "surface-writer_tmpl.cc"
