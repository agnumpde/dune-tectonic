#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/fufem/hdf5/singletonwriter.hh>

#include "frictionalboundary-writer.hh"
#include "restrict.hh"

template <class ProgramState, class GridView>
FrictionalBoundaryWriter<ProgramState, GridView>::FrictionalBoundaryWriter(
    HDF5::Grouplike &file, Vector const &vertexCoordinates,
    Patch const &frictionalBoundary)
    : group_(file, "frictionalBoundary"),
      frictionalBoundary_(frictionalBoundary),
      frictionalBoundaryDisplacementWriter_(group_, "displacement",
                                            frictionalBoundary.numVertices(),
                                            Vector::block_type::dimension),
      frictionalBoundaryVelocityWriter_(group_, "velocity",
                                        frictionalBoundary.numVertices(),
                                        Vector::block_type::dimension),
      frictionalBoundaryStateWriter_(group_, "state",
                                     frictionalBoundary.numVertices()),
      frictionalBoundaryCoefficientWriter_(group_, "coefficient",
                                           frictionalBoundary.numVertices()) {
  auto const frictionalBoundaryCoordinates =
      restrictToSurface(vertexCoordinates, frictionalBoundary);
  HDF5::SingletonWriter<2> frictionalBoundaryCoordinateWriter(
      group_, "coordinates", frictionalBoundaryCoordinates.size(),
      Vector::block_type::dimension);
  setEntry(frictionalBoundaryCoordinateWriter, frictionalBoundaryCoordinates);
}

template <class ProgramState, class GridView>
template <class Friction>
void FrictionalBoundaryWriter<ProgramState, GridView>::write(
    ProgramState const &programState, Friction &friction) {
  auto const frictionalBoundaryDisplacements =
      restrictToSurface(programState.u, frictionalBoundary_);
  addEntry(frictionalBoundaryDisplacementWriter_, programState.timeStep,
           frictionalBoundaryDisplacements);

  auto const frictionalBoundaryVelocities =
      restrictToSurface(programState.v, frictionalBoundary_);
  addEntry(frictionalBoundaryVelocityWriter_, programState.timeStep,
           frictionalBoundaryVelocities);

  auto const frictionalBoundaryStates =
      restrictToSurface(programState.alpha, frictionalBoundary_);
  addEntry(frictionalBoundaryStateWriter_, programState.timeStep,
           frictionalBoundaryStates);

  friction.updateAlpha(programState.alpha);
  auto const c = friction.coefficientOfFriction(programState.v);
  auto const frictionalBoundaryCoefficient =
      restrictToSurface(c, frictionalBoundary_);
  addEntry(frictionalBoundaryCoefficientWriter_, programState.timeStep,
           frictionalBoundaryCoefficient);
}

#include "frictionalboundary-writer_tmpl.cc"
