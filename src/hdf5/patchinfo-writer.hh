#ifndef SRC_HDF_PATCHINFO_WRITER_HH
#define SRC_HDF_PATCHINFO_WRITER_HH

#include <dune/istl/matrix.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/geometry/convexpolyhedron.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

#include "../one-body-problem-data/mygeometry.hh"

template <class LocalVector, class GridView> class GridEvaluator {
  using Element = typename GridView::Grid::template Codim<0>::Entity;

public:
  GridEvaluator(ConvexPolyhedron<LocalVector> const &weakPatch,
                GridView const &gridView);

  template <class Function>
  Dune::Matrix<typename Function::RangeType> evaluate(Function const &f) const;

  Dune::BlockVector<Dune::FieldVector<double, 1>> xCoordinates;
  Dune::BlockVector<Dune::FieldVector<double, 1>> zCoordinates;

private:
  std::vector<std::vector<std::pair<Element, LocalVector>>> localInfo;
};

template <class ProgramState, class VertexBasis, class GridView>
class PatchInfoWriter {
  using Vector = typename ProgramState::Vector;
  using LocalVector = typename Vector::block_type;
  using Patch = BoundaryPatch<GridView>;

public:
  PatchInfoWriter(HDF5::Grouplike &file, VertexBasis const &vertexBasis,
                  Patch const &frictionalBoundary,
                  ConvexPolyhedron<LocalVector> const &weakPatch);

  void write(ProgramState const &programState);

private:
  HDF5::Group group_;

  VertexBasis const &vertexBasis_;

  GridEvaluator<LocalVector, GridView> const gridEvaluator_;
  HDF5::SequenceIO<3> weakPatchGridVelocityWriter_;
};
#endif
