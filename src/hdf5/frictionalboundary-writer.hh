#ifndef SRC_HDF_FRICTIONALBOUNDARY_WRITER_HH
#define SRC_HDF_FRICTIONALBOUNDARY_WRITER_HH

#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/hdf5/sequenceio.hh>

template <class ProgramState, class GridView> class FrictionalBoundaryWriter {
  using ScalarVector = typename ProgramState::ScalarVector;
  using Vector = typename ProgramState::Vector;
  using Patch = BoundaryPatch<GridView>;

public:
  FrictionalBoundaryWriter(HDF5::Grouplike &file, Vector const &vertexCoordinates,
                           Patch const &frictionalBoundary);

  template <class Friction>
  void write(ProgramState const &programState, Friction &friction);

private:
  HDF5::Group group_;

  Patch const &frictionalBoundary_;

  HDF5::SequenceIO<2> frictionalBoundaryDisplacementWriter_;
  HDF5::SequenceIO<2> frictionalBoundaryVelocityWriter_;
  HDF5::SequenceIO<1> frictionalBoundaryStateWriter_;
  HDF5::SequenceIO<1> frictionalBoundaryCoefficientWriter_;
};
#endif
