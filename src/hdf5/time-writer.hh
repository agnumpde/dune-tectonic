#ifndef SRC_HDF_TIME_WRITER_HH
#define SRC_HDF_TIME_WRITER_HH

#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

template <class ProgramState> class TimeWriter {
public:
  TimeWriter(HDF5::Grouplike &file);
  void write(ProgramState const &programState);

private:
  HDF5::Grouplike &file_;
  HDF5::SequenceIO<0> relativeTimeWriter_;
  HDF5::SequenceIO<0> relativeTimeIncrementWriter_;
};
#endif
