#ifndef SRC_HDF_RESTART_HDF_HH
#define SRC_HDF_RESTART_HDF_HH

#include <dune/fufem/hdf5/file.hh>
#include <dune/fufem/hdf5/sequenceio.hh>

template <class ProgramState> class RestartIO {
  using ScalarVector = typename ProgramState::ScalarVector;
  using Vector = typename ProgramState::Vector;

public:
  RestartIO(HDF5::Grouplike &group, size_t vertexCount);

  void write(ProgramState const &programState);

  void read(size_t timeStep, ProgramState &programState);

private:
  HDF5::SequenceIO<2> displacementWriter_;
  HDF5::SequenceIO<2> velocityWriter_;
  HDF5::SequenceIO<2> accelerationWriter_;
  HDF5::SequenceIO<1> stateWriter_;
  HDF5::SequenceIO<1> weightedNormalStressWriter_;
  HDF5::SequenceIO<0> relativeTimeWriter_;
  HDF5::SequenceIO<0> relativeTimeIncrementWriter_;
};
#endif
