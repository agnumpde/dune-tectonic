#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "iteration-writer.hh"

IterationWriter::IterationWriter(HDF5::Grouplike &file)
    : group_(file, "iterations"),
      fpiSubGroup_(group_, "fixedPoint"),
      mgSubGroup_(group_, "multiGrid"),
      finalMGIterationWriter_(mgSubGroup_, "final"),
      finalFPIIterationWriter_(fpiSubGroup_, "final"),
      totalMGIterationWriter_(mgSubGroup_, "total"),
      totalFPIIterationWriter_(fpiSubGroup_, "total") {}

void IterationWriter::write(size_t timeStep,
                            IterationRegister const &iterationCount) {
  addEntry(finalMGIterationWriter_, timeStep,
           iterationCount.finalCount.multigridIterations);
  addEntry(finalFPIIterationWriter_, timeStep,
           iterationCount.finalCount.iterations);
  addEntry(totalMGIterationWriter_, timeStep,
           iterationCount.totalCount.multigridIterations);
  addEntry(totalFPIIterationWriter_, timeStep,
           iterationCount.totalCount.iterations);
}
