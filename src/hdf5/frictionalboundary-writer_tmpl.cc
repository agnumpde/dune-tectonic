#include "../explicitvectors.hh"
#include "../explicitgrid.hh"

#include "../program_state.hh"

using MyProgramState = ProgramState<Vector, ScalarVector>;
using MyFriction = GlobalFriction<Matrix, Vector>;

template class FrictionalBoundaryWriter<MyProgramState, GridView>;
template void FrictionalBoundaryWriter<MyProgramState, GridView>::write(
    MyProgramState const &programState, MyFriction &friction);
