#ifndef SRC_HDF_ITERATION_WRITER_HH
#define SRC_HDF_ITERATION_WRITER_HH

#include <dune/fufem/hdf5/sequenceio.hh>

#include "../time-stepping/adaptivetimestepper.hh"

class IterationWriter {
public:
  IterationWriter(HDF5::Grouplike &file);

  void write(size_t timeStep, IterationRegister const &iterationCount);

private:
  HDF5::Group group_;
  HDF5::Group fpiSubGroup_;
  HDF5::Group mgSubGroup_;

  HDF5::SequenceIO<0, size_t> finalMGIterationWriter_;
  HDF5::SequenceIO<0, size_t> finalFPIIterationWriter_;
  HDF5::SequenceIO<0, size_t> totalMGIterationWriter_;
  HDF5::SequenceIO<0, size_t> totalFPIIterationWriter_;
};
#endif
