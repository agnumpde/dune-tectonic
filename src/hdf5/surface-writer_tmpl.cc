#include "../explicitvectors.hh"
#include "../explicitgrid.hh"

#include "../program_state.hh"

using MyProgramState = ProgramState<Vector, ScalarVector>;

template class SurfaceWriter<MyProgramState, GridView>;
