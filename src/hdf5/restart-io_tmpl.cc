#include "../explicitvectors.hh"

#include "../program_state.hh"

using MyProgramState = ProgramState<Vector, ScalarVector>;

template class RestartIO<MyProgramState>;
