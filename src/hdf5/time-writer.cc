#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "time-writer.hh"

template <class ProgramState>
TimeWriter<ProgramState>::TimeWriter(HDF5::Grouplike &file)
    : file_(file),
      relativeTimeWriter_(file_, "relativeTime"),
      relativeTimeIncrementWriter_(file_, "relativeTimeIncrement") {}

template <class ProgramState>
void TimeWriter<ProgramState>::write(ProgramState const &programState) {
  addEntry(relativeTimeWriter_, programState.timeStep,
           programState.relativeTime);
  addEntry(relativeTimeIncrementWriter_, programState.timeStep,
           programState.relativeTau);
}

#include "time-writer_tmpl.cc"
