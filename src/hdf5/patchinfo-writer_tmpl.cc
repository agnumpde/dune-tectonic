#include "../explicitvectors.hh"
#include "../explicitgrid.hh"

#include "../program_state.hh"

using MyProgramState = ProgramState<Vector, ScalarVector>;
using P1Basis = P1NodalBasis<GridView, double>;
using MyFunction = BasisGridFunction<P1Basis, Vector>;

template class GridEvaluator<LocalVector, GridView>;
template Dune::Matrix<typename MyFunction::RangeType>
GridEvaluator<LocalVector, GridView>::evaluate(MyFunction const &f) const;

template class PatchInfoWriter<MyProgramState, P1Basis, GridView>;
