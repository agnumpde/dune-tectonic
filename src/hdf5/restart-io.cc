#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "restart-io.hh"

template <class ProgramState>
RestartIO<ProgramState>::RestartIO(HDF5::Grouplike &group, size_t vertexCount)
    : displacementWriter_(group, "displacement", vertexCount,
                          Vector::block_type::dimension),
      velocityWriter_(group, "velocity", vertexCount,
                      Vector::block_type::dimension),
      accelerationWriter_(group, "acceleration", vertexCount,
                          Vector::block_type::dimension),
      stateWriter_(group, "state", vertexCount),
      weightedNormalStressWriter_(group, "weightedNormalStress", vertexCount),
      relativeTimeWriter_(group, "relativeTime"),
      relativeTimeIncrementWriter_(group, "relativeTimeIncrement") {}

template <class ProgramState>
void RestartIO<ProgramState>::write(ProgramState const &programState) {
  addEntry(displacementWriter_, programState.timeStep, programState.u);
  addEntry(velocityWriter_, programState.timeStep, programState.v);
  addEntry(accelerationWriter_, programState.timeStep, programState.a);
  addEntry(stateWriter_, programState.timeStep, programState.alpha);
  addEntry(weightedNormalStressWriter_, programState.timeStep,
           programState.weightedNormalStress);
  addEntry(relativeTimeWriter_, programState.timeStep,
           programState.relativeTime);
  addEntry(relativeTimeIncrementWriter_, programState.timeStep,
           programState.relativeTau);
}

template <class ProgramState>
void RestartIO<ProgramState>::read(size_t timeStep,
                                   ProgramState &programState) {
  programState.timeStep = timeStep;
  readEntry(displacementWriter_, timeStep, programState.u);
  readEntry(velocityWriter_, timeStep, programState.v);
  readEntry(accelerationWriter_, timeStep, programState.a);
  readEntry(stateWriter_, timeStep, programState.alpha);
  readEntry(weightedNormalStressWriter_, timeStep,
            programState.weightedNormalStress);
  readEntry(relativeTimeWriter_, timeStep, programState.relativeTime);
  readEntry(relativeTimeIncrementWriter_, timeStep, programState.relativeTau);
}

#include "restart-io_tmpl.cc"
