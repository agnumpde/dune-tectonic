#ifndef SRC_HDF_SURFACE_WRITER_HH
#define SRC_HDF_SURFACE_WRITER_HH

#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/hdf5/sequenceio.hh>
#include <dune/fufem/hdf5/singletonwriter.hh>

template <class ProgramState, class GridView> class SurfaceWriter {
  using Vector = typename ProgramState::Vector;
  using Patch = BoundaryPatch<GridView>;

public:
  SurfaceWriter(HDF5::Grouplike &file, Vector const &vertexCoordinates,
                Patch const &surface);

  void write(ProgramState const &programState);

private:
  HDF5::Group group_;

  Patch const &surface_;

  HDF5::SequenceIO<2> surfaceDisplacementWriter_;
  HDF5::SequenceIO<2> surfaceVelocityWriter_;
};
#endif
