#ifndef SRC_MATRICES_HH
#define SRC_MATRICES_HH

template <class Matrix> struct Matrices {
  Matrix elasticity;
  Matrix damping;
  Matrix mass;
};
#endif
