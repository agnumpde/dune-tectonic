#ifndef SRC_HDF5_WRITER_HH
#define SRC_HDF5_WRITER_HH

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/geometry/convexpolyhedron.hh>
#include <dune/fufem/hdf5/file.hh>

#include "hdf5/frictionalboundary-writer.hh"
#include "hdf5/iteration-writer.hh"
#include "hdf5/patchinfo-writer.hh"
#include "hdf5/surface-writer.hh"
#include "hdf5/time-writer.hh"

template <class ProgramState, class VertexBasis, class GridView>
class HDF5Writer {
private:
  using Vector = typename ProgramState::Vector;
  using Patch = BoundaryPatch<GridView>;

  using LocalVector = typename Vector::block_type;

public:
  HDF5Writer(HDF5::Grouplike &file, Vector const &vertexCoordinates,
             VertexBasis const &vertexBasis, Patch const &surface,
             Patch const &frictionalBoundary,
             ConvexPolyhedron<LocalVector> const &weakPatch)
      : file_(file),
        iterationWriter_(file_),
        timeWriter_(file_),
#if MY_DIM == 3
        patchInfoWriter_(file_, vertexBasis, frictionalBoundary, weakPatch),
#endif
        surfaceWriter_(file_, vertexCoordinates, surface),
        frictionalBoundaryWriter_(file_, vertexCoordinates,
                                  frictionalBoundary) {
  }

  template <class Friction>
  void reportSolution(ProgramState const &programState,
                      // for the friction coefficient
                      Friction &friction) {
    timeWriter_.write(programState);
#if MY_DIM == 3
    patchInfoWriter_.write(programState);
#endif
    surfaceWriter_.write(programState);
    frictionalBoundaryWriter_.write(programState, friction);
  }

  void reportIterations(ProgramState const &programState,
                        IterationRegister const &iterationCount) {
    iterationWriter_.write(programState.timeStep, iterationCount);
  }

private:
  HDF5::Grouplike &file_;

  IterationWriter iterationWriter_;
  TimeWriter<ProgramState> timeWriter_;
#if MY_DIM == 3
  PatchInfoWriter<ProgramState, VertexBasis, GridView> patchInfoWriter_;
#endif
  SurfaceWriter<ProgramState, GridView> surfaceWriter_;
  FrictionalBoundaryWriter<ProgramState, GridView> frictionalBoundaryWriter_;
};
#endif
